function mostrar(dados) {
    let divResposta = document.querySelector("#resultado");

    while (divResposta.firstChild) {
        divResposta.removeChild(divResposta.firstChild);
    }

    if(dados.error_message){
        let htmlErro = document.createElement("p");
        htmlErro.innerHTML = "Erro ao obter os dados do servidor.";

        divResposta.appendChild(htmlErro);
    }
    else{
        let linkImagem = dados.message;

        let htmlImagem = document.querySelector("#resultado");
        htmlImagem.src = linkImagem
    }
}

function extrair(resposta) {
    if(resposta.ok){
        return resposta.json();
    }
}

function buscarDog(){
    const url = "https://dog.ceo/api/breeds/image/random";
    fetch(url).then(extrair).then(mostrar);
}

let botao = document.querySelector("button");
botao.onclick = buscarDog;
